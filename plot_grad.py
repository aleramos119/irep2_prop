import numpy as np
from scipy import integrate

from scipy.optimize import curve_fit

import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
#%matplotlib qt
plt.style.use(['ggplot'])


import sys
#sys.path.append('/home/ar612/Documents/Python_modules/')
sys.path.append('/cluster/data/ar612/scripts/python/')
import mol

##### Defining functions ############################
def Vas(x,vmax,a,b):
    return (vmax/a**4)*(x**2-a**2)**2+b*x/a


def gauss(x,g,b,x0):
    return g*np.e**(-b*(x-x0)**2)

def x_gauss(x,g,b,x0):
    return x*g*np.e**(-b*(x-x0)**2)

def sum_gauss(x,g,b,x0):
    sum=0
    for i in range(0,len(g)):
        sum=sum + gauss(x,g[i],b[i],x0[i])
    return sum


def integ(f,xi,xf):
    i=integrate.quad(f,xi,xf)
    return i

######### Defining gaussians coeficients ####################
vmax=0
a=1

g=np.array([31.00001469*vmax,-1.52881858*vmax,-1.52881858*vmax,31.00001469*vmax,1.34845505*vmax])  ##/*!< Gaussian potential expansion height coefficients */
b=np.array([1.39712198*pow(a,-2),1.65840237*pow(a,-2),1.65840237*pow(a,-2),1.39712198*pow(a,-2),0.*pow(a,-2)]) ##     /*!<  Gaussian potential expansion wide coefficients */
xp=np.array([-2.98085171*a,-1.14213435*a,1.14213435*a,2.98085171*a,0.*a])      ##   /*!<  Gaussian potential expansion center coefficients */




######## Ploting the Potential ##################################
xmax=4
x=np.linspace(-xmax,xmax,1000)
#plt.plot(x,Vas(x,vmax,a,0),x,sum_gauss(x,g,b,xp))



#plt.plot(x,Vas(x,vmax,a,0),x,sum_gauss(x,g,b,xp),x, gauss(x,(2*alfa/np.pi)**(1/2.), 2*alfa , dat[3,0]) )





###%%
#%matplotlib qt
### Creating the subfigures
fig=plt.figure(1,figsize=(10,8))
plt.style.use(['ggplot'])


###Ploting the first figure
i0=3
efield_file_pattern="results/gradn_$$.txt"
gradn_file=efield_file_pattern.replace("$$",str(i0))

efield_file_pattern="results/gradn_1_$$.txt"
gradn_1_file=efield_file_pattern.replace("$$",str(i0))

efield_file_pattern="results/sn_$$.txt"
sn_file=efield_file_pattern.replace("$$",str(i0))

efield_file_pattern="results/sn_1_$$.txt"
sn_1_file=efield_file_pattern.replace("$$",str(i0))


tgradn,gradn=mol.fast_load(gradn_file,np.array([0,1]),1000).T
tgradn_1,gradn_1=mol.fast_load(gradn_1_file,np.array([0,1]),1000).T
tsn,sn=mol.fast_load(sn_file,np.array([0,1]),1000).T
tsn_1,sn_1=mol.fast_load(sn_1_file,np.array([0,1]),1000).T


# spline_file_pattern="results/spline_$$.txt"
# spline_file=spline_file_pattern.replace("$$",str(0+1))
# tspline,espline=mol.fast_load(spline_file,np.array([0,1]),1000).T

fs2au=41.3414

efield_file=":o <8"
tsn=tsn/fs2au
#tspline=tspline/fs2au

lim1=0.01
lim2=0.01
lim3=0.01
lim4=0.01
lim5=0.00002
lim6=0.00005
##%%
#%matplotlib qt
plt.subplot(221)
plt.axis([min(abs(tsn)), max(abs(tsn)), -lim1, lim1])
plt.plot(tsn,gradn, label=efield_file)
plt.xlabel("t (fs)",fontweight='bold')
plt.ylabel("gradn",fontweight='bold')
#plt.legend()
plt.tick_params(labelsize=14)

plt.subplot(222)
plt.axis([min(abs(tsn)), max(abs(tsn)), -lim2, lim2])
plt.plot(tsn,gradn_1, label=efield_file)
plt.xlabel("t (fs)",fontweight='bold')
plt.ylabel("gradn_1",fontweight='bold')
#plt.legend()
plt.tick_params(labelsize=14)




plt.subplot(223)
plt.axis([min(abs(tsn)), max(abs(tsn)), -lim3, lim3])
plt.plot(tsn,sn, label=efield_file)
plt.xlabel("t (fs)",fontweight='bold')
plt.ylabel("sn",fontweight='bold')
#plt.legend()
plt.tick_params(labelsize=14)


plt.subplot(224)
plt.axis([min(abs(tsn)), max(abs(tsn)), -lim4, lim4])
plt.plot(tsn,sn_1, label="p0",color='b')
plt.xlabel("t (fs)",fontweight='bold')
plt.ylabel("sn_1 (au)",fontweight='bold')
#plt.legend()
plt.tick_params(labelsize=14)


#
# plt.subplot(235)
# plt.axis([min(abs(te)), max(abs(te)), 0, lim5])
# plt.plot(te,px, label="p0",color='b')
# plt.xlabel("t (fs)",fontweight='bold')
# plt.ylabel("a (au)",fontweight='bold')
# #plt.legend()
# plt.tick_params(labelsize=14)
#
#
# plt.subplot(236)
# plt.axis([min(abs(te)), max(abs(te)), -lim6, lim6])
# plt.plot(te,pp0, label="p0",color='b')
# plt.xlabel("t (fs)",fontweight='bold')
# plt.ylabel("b (au)",fontweight='bold')
# #plt.legend()
# plt.tick_params(labelsize=14)







##%%
imax=int(sys.argv[1])-1
## Set slide axes
axi_val = plt.axes([0.25, .00, 0.50, 0.02])
# Slider time
si_val = Slider(axi_val, 'i', 3, imax,valinit=3,valstep=2)


def update_i(val):

    i0=int(si_val.val)

    efield_file_pattern="results/gradn_$$.txt"
    gradn_file=efield_file_pattern.replace("$$",str(i0))

    efield_file_pattern="results/gradn_1_$$.txt"
    gradn_1_file=efield_file_pattern.replace("$$",str(i0))

    efield_file_pattern="results/sn_$$.txt"
    sn_file=efield_file_pattern.replace("$$",str(i0))

    efield_file_pattern="results/sn_1_$$.txt"
    sn_1_file=efield_file_pattern.replace("$$",str(i0))


    tgradn,gradn=mol.fast_load(gradn_file,np.array([0,1]),1000).T
    tgradn_1,gradn_1=mol.fast_load(gradn_1_file,np.array([0,1]),1000).T
    tsn,sn=mol.fast_load(sn_file,np.array([0,1]),1000).T
    tsn_1,sn_1=mol.fast_load(sn_1_file,np.array([0,1]),1000).T


    fs2au=41.3414

    tsn=tsn/fs2au


    #%matplotlib qt
    plt.subplot(221)
    plt.cla()
    plt.axis([min(abs(tsn)), max(abs(tsn)), -lim1, lim1])
    plt.plot(tsn,gradn, label=efield_file)
    plt.xlabel("t (fs)",fontweight='bold')
    plt.ylabel("gradn (au)",fontweight='bold')
    #plt.legend()
    plt.tick_params(labelsize=14)

    plt.subplot(222)
    plt.cla()
    plt.axis([min(abs(tsn)), max(abs(tsn)), -lim2, lim2])
    plt.plot(tsn,gradn_1, label=efield_file)
    plt.xlabel("t (fs)",fontweight='bold')
    plt.ylabel("gradn_1",fontweight='bold')
    #plt.legend()
    plt.tick_params(labelsize=14)




    plt.subplot(223)
    plt.cla()
    plt.axis([min(abs(tsn)), max(abs(tsn)), -lim3, lim3])
    plt.plot(tsn,sn, label=efield_file)
    plt.xlabel("t (fs)",fontweight='bold')
    plt.ylabel("sn",fontweight='bold')
    #plt.legend()
    plt.tick_params(labelsize=14)


    plt.subplot(224)
    plt.cla()
    plt.axis([min(abs(tsn)), max(abs(tsn)), -lim4, lim4])
    plt.plot(tsn,sn_1, label="p0",color='b')
    plt.xlabel("t (fs)",fontweight='bold')
    plt.ylabel("sn_1",fontweight='bold')
    #plt.legend()
    plt.tick_params(labelsize=14)


# call update function on slider value change
si_val.on_changed(update_i)

plt.show()
###%%
# import os
# os.getcwd()
# c=1e-11
# iter,J=mol.fast_load("results/iter_monit.txt",np.array([0,3]),1000).T
# dJ=J[1]-J[0]
# dJ
# t_0,e_0=mol.fast_load("results/t_monitor_0.txt",np.array([0,1]),1000).T
#
# t_grad,grad_0=mol.fast_load("results/t_monitor_1.txt",np.array([0,2]),1000).T
#
# t_1,e_1=mol.fast_load("results/t_monitor_2.txt",np.array([0,1]),1000).T
#
# -c*np.dot(grad_0,grad_0)
#
#
# #
# # efield_file="results/t_et_17.txt"
# # tspline,espline=mol.fast_load(efield_file,np.array([0,1]),1000).T
#
# plt.plot(iter,J,'o')
#
# plt.plot(t_0,(e_1-e_0)/c)
# plt.plot(t_grad,grad_0)
#
# plt.plot(t_grad,grad_0)
# plt.plot(tspline,espline,'b-')
