
class dif_eq
{
  private : 
      double q, m, vmax, a;
      double a0;
      double T;
      int wf; //This is an indicator if I'm propagating the wavefunction (1) or the lagrange multipliers (-1). This is important for E12(t,Y)
      int im_re;   //If I do imaginary time (1) or real time(2) propagation

      //This are the gaussian expantion of the potential
      size_t n_gauss = 5;                                                                                                                  /*!< Number of gaussians used for expanding the external potential */
      double g[5] = {31.00001469 * vmax, -1.52881858 * vmax, -1.52881858 * vmax, 31.00001469 * vmax, 1.34845505 * vmax};                   /*!< Gaussian potential expansion height coefficients */
      double b[5] = {1.39712198 * pow(a, -2), 1.65840237 * pow(a, -2), 1.65840237 * pow(a, -2), 1.39712198 * pow(a, -2), 0. * pow(a, -2)}; /*!<  Gaussian potential expansion wide coefficients */
      double xp[5] = {-2.98085171 * a, -1.14213435 * a, 1.14213435 * a, 2.98085171 * a, 0. * a};                                           /*!<  Gaussian potential expansion center coefficients */

      //This function is the previous electric field. I define it in the initialization list of the class constructor
      std::function<double(double)> E; //Declaring the function E(t)


      /**
       * @brief This is a function that defines the shape of the pulse
       * 
       * @param t The current instant of time
       * @param T The final time of the propagation 
       * @param a0 The penalty factor to avoid unfisical large electric fields
       * @return double Returns the evaluation of the function
       */
      double s(double t, double T, double a0)
      {
        //return pow( sin(PI*t/T) ,2.)/a0;
        return 1;
      }

      double Bp(double a, double x0, double xp, double bp)
      {
        return (2 * a * bp) * pow(x0 - xp, 2) / (2 * a + bp);
      }

      double Dp(double a, double x0, double xp, double bp, double gp)
      {
        return gp * exp(-Bp(a, x0, xp, bp)) * pow(2 * a / (2 * a + bp), 3. / 2.);
      }

      double dV_da(double a, double b, double x0, double gp, double bp, double xp)
      {
        return Dp(a, x0, xp, bp, gp) * (bp / (4 * pow(a, 2)) - pow(bp * (x0 - xp), 2) / (a * (2 * a + bp)));
      }

      double dV_dx(double a, double b, double x0, double gp, double bp, double xp)
      {
        return -2 * Dp(a, x0, xp, bp, gp) * bp * (x0 - xp);
      }
      double V(double a, double b, double x0, double gp, double bp, double xp)
      {
        return gp * exp(-Bp(a, x0, xp, bp)) * pow(2 * a / (2 * a + bp), 1. / 2.);
      }

  public :
        static const int n_monit=7;

        dif_eq(double q, double m, double vmax, double a, int im_re, std::function<double(double)> E, double a0, double T, int wf) : q(q), m(m), vmax(vmax), a(a), im_re(im_re), a0(a0), T(T), E(E), wf(wf)
        {}


      /**
       * @brief This an overload of the () operator so I can pass this function to Solve_DEq
       * 
       * @param f This is a pointer where I store the evaluation of the function
       * @param t The current instant of time 
       * @param Y The current values of the wavefunction parameters
       * @param n_eq The number of equations/parameters that I have
       */
      void operator()(double *der, double *monit, size_t n_monit, double t, double *Y, size_t n_eq)
      {

        if ((im_re != 1 && im_re != 2))
        {
          throw invalid_argument("!!!!!!!!!! im_re have to be 1 or 2 in class dif_eq !!!!!!!!!!!!!!!!");
        }

        double a1, b1, x01, p01, a2, b2, x02, p02; //The parameters that characterizes current wavefunction (1) propagation and previous wavefunction (2)
        double re2;                                //This is equal to 2*Re[C3-C2^2/(4*C1)]
        double im_c2_c1;                           //This is equal to Im[C2*/C1*]
        double gam1_gam2;                          //Iqual to |Gam1* + Gam2|
        double gam1_2;
        double gam2_2;

        //This are the variables that I will monitor at each step
        double E12;
        double pot;
        double over;

        double A_p1, B_p1, D_p1; /*!< Some auxiliar variables to improve readability of current wavefunction (1)*/
        double A_p2, B_p2, D_p2; /*!< Some auxiliar variables to improve readability of previous wavefunction (2)*/


        //Asigning names to the Y vector
        for (int i = 0; i < 1; i++)
        {
          a1 = Y[i + 0];
          b1 = Y[i + 1];
          x01 = Y[i + 2];
          p01 = Y[i + 3];

          a2 = Y[i + 4];
          b2 = Y[i + 5];
          x02 = Y[i + 6];
          p02 = Y[i + 7];
        }

        // Calculating current electric field ////////////////////////
        gam1_gam2 = sqrt(pow(a1 + a2, 2.) + pow(b1 - b2, 2.));
        gam1_2 = pow(a1, 2.) + pow(b1, 2.);
        gam2_2 = pow(a2, 2.) + pow(b2, 2.);

        re2 = -2 * ((a1 * gam2_2 + a2 * gam1_2) * pow(x01 - x02, 2.) + (a1 * b2 + a2 * b1) * (p01 - p02) * (x01 - x02) + (a1 + a2) * pow(p01 - p02, 2.) / 4) / pow(gam1_gam2, 2);

        im_c2_c1 = (2 * (a1 * b2 + a2 * b1) * (x02 - x01) + (a1 + a2) * (p02 - p01)) / pow(gam1_gam2, 2);

        E12 = q * s(t, T, a0) * (sqrt(a1 * a2) / gam1_gam2) * exp(re2) * im_c2_c1;

        E12=E12*wf; // The field is antysimetric with respect to the propagation of the field or the lagrange multipliers
        // Calculating current electric field ///////////////////////
        
        // Calculating overlap
        over = sqrt(4 * a1 * a2) * exp(re2) / gam1_gam2;
        // Calculating overlap

        for (size_t i = 0; i < 1; i++) //Runs through all equations and assign the value of each parameter derivative \dot{Y_i}
        {

          double dv_dx01 = -q * E12;
          double dv_dx02 = -q * E(t);
          
          double dv_da1 = 0;
          double dv_da2 = 0;

          //pot = -q * x01 * E12;
          pot=0;

          for (size_t p = 0; p < n_gauss; p++) //This for runs through every gaussian in the expanded external potential
          {
            //The derivative respect to alfa and x of the first propagation 
            dv_da1 = dv_da1 + dV_da(a1, b1, x01, g[p], b[p], xp[p]);
            dv_dx01 = dv_dx01 + dV_dx(a1, b1, x01, g[p], b[p], xp[p]);

            //The derivative respect to alfa and x of the second propagation 
            dv_da2 = dv_da2 + dV_da(a2, b2, x02, g[p], b[p], xp[p]);
            dv_dx02 = dv_dx02 + dV_dx(a2, b2, x02, g[p], b[p], xp[p]);

            //The potential of the first propagation
            pot = pot + V(a1, b1, x01, g[p], b[p], xp[p]);
          }

          if (im_re == 1)
          {
            der[i + 4] = -2 * (pow(a2, 2) - pow(b2, 2)) / m - 4 * pow(a2, 2) * dv_da2; //Ecuation for imaginary propagation of alfa
            der[i + 5] = -(4 * a2 * b2) / m;                                           //Ecuation for imaginary propagation of beta

            der[i + 6] = (b2 * p02) / (a2 * m) - dv_dx02 / (2 * a2);                        //Ecuation for imaginary propagation of x0
            der[i + 7] = -2 * (pow(a2, 2) + pow(b2, 2)) * p02 / (a2 * m) - b2 * dv_dx02 / a2; //Ecuation for imaginary propagation of px0
          }

          else if (im_re == 2)
          {
            der[i + 0] = (4 * a1 * b1) / m;                                          //Ecuation for real propagation of alfa
            der[i + 1] = -2 * (pow(a1, 2) - pow(b1, 2)) / m - 4 * pow(a1, 2) * dv_da1; //Ecuation for real propagation of beta

            der[i + 2] = p01 / m; //Ecuation for real propagation of x0
            der[i + 3] = -dv_dx01; //Ecuation for real propagation of p0

            der[i + 4] = (4 * a2 * b2) / m;                                          //Ecuation for real propagation of alfa
            der[i + 5] = -2 * (pow(a2, 2) - pow(b2, 2)) / m - 4 * pow(a2, 2) * dv_da2; //Ecuation for real propagation of beta

            der[i + 6] = p02 / m; //Ecuation for real propagation of x0
            der[i + 7] = -dv_dx02; //Ecuation for real propagation of p0
          }
        }

        monit[0]=E12;
        monit[1]=pot;
        monit[2]=over;
        if (wf==1)
        {
          //coordinates
          monit[3] = x01;
          monit[4] = x02;

          //momentum
          monit[5] = p01;
          monit[6] = p02;
        }
        if (wf == -1)
        {
          //coordinates
          monit[3] = x02;
          monit[4] = x01;

          //momentum
          monit[5] = p02;
          monit[6] = p01;
        }
      }



};
