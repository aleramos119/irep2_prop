
#%%
import numpy as np

import matplotlib.pyplot as plt
from itertools import islice
import sys
sys.path.append('$PYDIR')
sys.path.append('/home/ar612/Documents/Python_modules')
import mol




#########################################################################%%
def plot_e(dist1,e1,dist2,e2,dist3,e3,project):
    plt.plot(dist1,e1,'b-o',linewidth=2.5,label="b3lyp",color="tab:blue")
    plt.plot(dist2,e2,'b-o',linewidth=2.5,label="dftb",color="tab:red")
    plt.plot(dist3,e3,'bo',label="ock.",color="tab:green")
    plt.xlabel("reac_path (A)",fontweight='bold')
    plt.tick_params(labelsize=14)
    plt.ylabel('E (kcal/mol)',fontweight='bold')
    plt.legend()
    plt.grid(True)
    plt.show()



############################################################################%%


def main (xyz_inp_file1,xyz_inp_file2,dist3,e3,project,n_atm):

    ## Reading energies and reaction path from movie file. e is in Hartrees and r in armstrong

    e2,r2,atm_names2=mol.e_r_atm_names_from_xyz(xyz_inp_file2,n_atm)
    e1,r1,atm_names1=mol.e_r_atm_names_from_xyz(xyz_inp_file1,n_atm)



    ## Expresing energies in kcal/mol and using the first image as reference
    e1=(e1-e1[0])*mol.Eh2kcalmol
    e2=(e2-e2[0])*mol.Eh2kcalmol

    dist1=np.array(range(0,len(e1)))
    dist2=np.array(range(0,len(e2)))

    dr1,dist1=mol.path_dist(r1)
    dr2,dist2=mol.path_dist(r2)

    np.array([dist1,e1]).T


    mol.write_matrix_to_file(np.array([dist1,e1]).T,'','',"/home/ar612/Documents/cp2k_organized/reac_path/"+project1+"/"+project1+"_dist_ener.txt")

    plot_e(dist1,e1,dist2,e2,dist3,e3,project)



###########################################################################%%


if __name__== "__main__":

    project1="r20_ts1_p_band_b3lyp_40_rep"
    project2="r20_ts1_p_band_dftb_40_rep"

    e3=np.array([0,33.638168012432288,-27.103197363234358])
    dist3=np.array([0,13.1,23.9])

    n_atm=33
    %matplotlib qt
    #
    # xyz_inp_file=sys.argv[1]
    # n_atm=int(sys.argv[2])


    for i in range(30,40):
        xyz_inp_file_pattern="/cluster/data/ar612/cp2k_organized/reac_path/"+project+"/"+project+"-pos-Replica_nr_$$-1.xyz"
        xyz_inp_file=xyz_inp_file_pattern.replace("$$",str(i))
        main(xyz_inp_file,project,n_atm)

    xyz_inp_file1="/home/ar612/Documents/cp2k_organized/reac_path/"+project1+"/"+project1+"_movie.xyz"
    xyz_inp_file2="/home/ar612/Documents/cp2k_organized/reac_path/"+project2+"/"+project2+"_movie.xyz"
    xyz_inp_file="/cluster/data/ar612/cp2k_organized/geo_opt/t3_b3lyp_geo_opt/t3_b3lyp_geo_opt-pos-1.xyz"
    main(xyz_inp_file1,xyz_inp_file2,project,n_atm)
