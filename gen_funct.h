    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////// Expresions depending on one state //////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    # define PI          3.1415926535897932384626433 /*!< PI value taken from Wikipedia*/
    #define ps2au 41341.373337		//Un picosegundo en unidades atómicas
    #define fs2au 41.341373337   //Un picosegundo en unidades atómicas
    #define au2fs 0.024188843265
  
    template<class type1, class type2>
    void H_dot_a(type1 *b, type2 **H, type1 *a, size_t Emax)
    {
        for (size_t i=0; i<Emax; i++)
        {
            b[i]=0;
            b[i+Emax]=0;
            for (size_t j=0; j<Emax; j++)
            {
                b[i]=b[i]+H[i][j]*a[j+Emax];
                b[i+Emax]=b[i+Emax]-H[i][j]*a[j];
            }
        }

    }
      
      
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////// Expresions depending on two states //////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    template<class type1, class type2>
    type1 cross_correl_2(vector<type1> a, vector<type2>  b)
    {
      size_t nstate_a=a.size();
      size_t nstate_b=b.size();
      if (nstate_a != nstate_b)
           throw invalid_argument("*********** ERROR *************   cross_correl_2: both states vectors must have the same dimension");

      if (nstate_a % 2 !=0)
           throw invalid_argument("*********** ERROR *************   cross_correl_2: nstates_a must be a multiple of 2");

      size_t Emax= nstate_a/2;

      type1 re=0;
      type1 im=0; 
      for (size_t i=0; i<Emax; i++)
      {
          re = re + a[i]*b[i] + a[i+Emax]*b[i+Emax];
          im = im + a[i]*b[i+Emax] - a[i+Emax]*b[i];
      }

      return pow(re,2.)+pow(im,2.);
    }


    template<class type1>
    type1 norm(vector<type1> a)
    {
       return pow(cross_correl_2(a,a),1/4.);
    }

    template<class type1>
    void normalize(vector<type1> &a)
    {
      size_t nstate_a=a.size();
      if (nstate_a % 2 !=0)
           throw invalid_argument("*********** ERROR *************   cross_correl_2: nstates_a must be a multiple of 2");
      
      size_t Emax= nstate_a/2;

      double norm_a=norm(a);
      for (size_t i=0; i<2*Emax; i++)
      {
        a[i]=a[i]/norm_a;
      }
    }

  


