
class dif_eq
{
  private : 
      int for_back; //This is an indicator if I'm propagating forward (1) or backwards (-1). This is important for E12. In the forward propagation 
      string im_re;   //If I do imaginary time (1) or real time(2) propagation
      string int_normal;
      double grad_step;


      //This function is the previous electric field. I define it in the initialization list of the class constructor
      std::function<double(double)> E; //Declaring the function E(t)

      //These arrays are to storage the dipole matrix and eigenvalues
      vector<vector<double>> mu11;  //Dipole matrix
      vector<vector<double>> mu12;  //Dipole matrix
      vector<vector<double>> mu22;  //Dipole matrix
      
      vector<vector<double>> Q11;  //Coupling matrix
      vector<vector<double>> Q12;  //Coupling matrix
      vector<vector<double>> Q22;  //Coupling matrix

      vector<vector<double>> P12;  //Coupling matrix

      

      vector<vector<double>> eigen1;   //Diagonal version of eigen energies
      vector<vector<double>> eigen2;   //Diagonal version of eigen energies
      vector<vector<double>> eigen;
      int Emax1;
      int Emax2;
      int Emax;



    template<class type1, class type2>
    void H_dot_a(type1 *b, type2 **ReH, type2 **ImH, type1 *a, size_t Emax)
    {
        for (size_t i=0; i<Emax; i++)
        {
            b[i]=0;
            b[i+Emax]=0;
            for (size_t j=0; j<Emax; j++)
            {
                b[i]=b[i] + ( ReH[i][j]*a[j+Emax] + ImH[i][j]*a[j] );
                b[i+Emax]=b[i+Emax] + ( - ReH[i][j]*a[j] + ImH[i][j]*a[j+Emax] );
            }
        }
    }



    public:
      //static const int n_monit = 30;

      dif_eq(System sys, string im_re, string int_normal, std::function<double(double)> E) : mu11(sys.mu11), mu12(sys.mu12), mu22(sys.mu22), Q11(sys.Q11), Q12(sys.Q12), Q22(sys.Q22), P12(sys.P12), eigen1(sys.E1), eigen2(sys.E2), eigen(sys.E), im_re(im_re), int_normal(int_normal), E(E)
      {
        Emax1=eigen1.size();
        Emax2=eigen2.size();
        Emax=Emax1+Emax2;
      }




      /**
       * @brief This is an overload of the () operator so I can pass this function to Solve_DEq
       * 
       * @param f This is a pointer where I store the evaluation of the function
       * @param t The current instant of time 
       * @param Y The current values of the wavefunction parameters
       * @param n_eq The number of equations/parameters that I have
       */
      void operator()(double *der, double *monit, size_t n_monit, double t, double *Y, size_t n_eq)
      {

        if ((im_re != "im" && im_re != "re"))
        {
          throw invalid_argument("!!!!!!!!!! im_re have to be 1 or 2 in class dif_eq !!!!!!!!!!!!!!!!");
        }
 
        //print_matrix(eigen,Emax,Emax);

        double** H;
        double** ReH;
        double** ImH;
        double wij;
        matrix_allocation(ReH,Emax,Emax);
        matrix_allocation(ImH,Emax,Emax);
        matrix_allocation(H,Emax,Emax);



        if (im_re == "im")
        {

        }

        if (im_re == "re" and int_normal == "int")
        {
            for (size_t i=0; i<Emax; i++)
            {
                for (size_t j=i; j<Emax; j++)
                {
                    wij=eigen[i][i]-eigen[j][j];
                    if (i< Emax1 && j<Emax1)
                    {
                      ReH[i][j]=( -mu11[i][j]*E(t) +  Q11[i][j] ) *cos(wij*t);
                      ImH[i][j]=( -mu11[i][j]*E(t) +  Q11[i][j] ) *sin(wij*t);

                      ReH[j][i]=ReH[i][j];
                      ImH[j][i]=-ImH[i][j];
                    }
                    if (i< Emax1 && j>=Emax1)
                    {
                      ReH[i][j]=( -mu12[i][j-Emax1]*E(t) + P12[i][j-Emax1] + Q12[i][j-Emax1] ) *cos(wij*t);
                      ImH[i][j]=( -mu12[i][j-Emax1]*E(t) + P12[i][j-Emax1] + Q12[i][j-Emax1] ) *sin(wij*t);

                      ReH[j][i]=ReH[i][j];
                      ImH[j][i]=-ImH[i][j];
                    }
                    if (i>= Emax1 && j>=Emax1)
                    {
                      ReH[i][j]=( -mu22[i-Emax1][j-Emax1]*E(t) + Q22[i-Emax1][j-Emax1] ) *cos(wij*t);
                      ImH[i][j]=( -mu22[i-Emax1][j-Emax1]*E(t) + Q22[i-Emax1][j-Emax1] ) *sin(wij*t);

                      ReH[j][i]=ReH[i][j];
                      ImH[j][i]=-ImH[i][j];
                    }

                    // if (i==j)
                    // cout<<"  "<<ImH[i][j];
                }
            }
            H_dot_a(der, ReH,ImH, Y,Emax);
        }

        // if (im_re == "re" and int_normal == "normal")
        // {
        //   for (size_t i=0; i<Emax; i++)
        //   {
        //       for (size_t j=0; j<Emax; j++)
        //       {
        //           H[i][j]=eigen[i][j] - mu[i][j]*E(t);
        //           if (i< Emax1 && j<Emax1)
        //           {
        //             H[i][j]=eigen[i][j] - mu11[i][j]*E(t);
        //           }
        //           if (i< Emax1 && j>=Emax1)
        //           {
        //             H[i][j]=eigen[i][j] - ( mu12[i][j-Emax1]*E(t) - P12[i][j-Emax1] - Q12[i][j-Emax1] )**E(t);
        //           }
        //           if (i>= Emax1 && j<Emax1)
        //           {
        //             H[i][j]=eigen[i][j] - ( mu21[i-Emax1][j]*E(t) - P21[i-Emax1][j] - Q21[i-Emax1][j] )*E(t);
        //           }
        //           if (i>= Emax1 && j>=Emax1)
        //           {
        //             H[i][j]=eigen[i][j] - mu22[i-Emax1][j-Emax1]*E(t);
        //           }
        //           //cout<<endl<<H[i][j]<<"  "<<eigen[i][j]<<"  "<<mu[i][j]<<"  "<<E(t)<<endl;
        //       }
        //   }
        //   H_dot_a(der, H, Y,Emax);
        // }


        // matrix_delete(H,Emax);
        matrix_delete(ReH,Emax);
        matrix_delete(ImH,Emax);
        matrix_delete(H,Emax);

        monit[0]=E(t);
        for (size_t k=0; k<Emax;k++)
        {
          monit[k+1]=Y[k]*Y[k] + Y[k+Emax]*Y[k+Emax];
          //cout<<endl<<monit[k];
        }
        //monit[30] = Y[0];
        //monit[31] = Y[1];


        // monit[0] = E(t);
        // monit[1] = Y[0]*Y[0] + Y[0+Emax]*Y[0+Emax];
        // monit[2] = Y[1]*Y[1] + Y[1+Emax]*Y[1+Emax];
        // monit[3] = Y[2]*Y[2] + Y[2+Emax]*Y[2+Emax];
        // monit[4] = Y[3]*Y[3] + Y[3+Emax]*Y[3+Emax];
        // monit[5] = Y[4]*Y[4] + Y[4+Emax]*Y[4+Emax];
        // monit[6] = Y[5]*Y[5] + Y[5+Emax]*Y[5+Emax];
   
          
      }

};
